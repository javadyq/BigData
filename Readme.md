## 概述
-----------------
> 提升鲲鹏生态大数据竞争力相关的大数据相关代码和patch。

项目包含hadoop、hbase等相关性能或功能提升补丁。

### Hadoop部分 ###
> hadoop相关的代码和补丁。

目录路径：- [hadoop](hadoop)

**（1）国密SM4**
hadoop patch：- [patch](hadoop/sm4-crypto/patch/add-hadoop-sm4-support.patch)

**（2）hadoop2.7.3 NUMA 特性**
NUMA patch：- [patch](hadoop/hadoop-numa/patch/0001-Hadoop2.7.3-NUMA-Aware.patch)

### 2、HBase部分 ###
> hbase相关的代码和补丁。

**（1）HBase1.3.1读取锁优化 **
HBase patch：-[patch](hbase/ReadWriteLock/patch/hbase.patch)
